<!DOCTYPE html>
<html lang="en">
<head>
	<title>Aviva</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/animate.css">
	
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/magnific-popup.css">

	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/jquery.timepicker.css">
	
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url('public'); ?>/css/chatbot.css">
</head>
<body>
	<div class="py-1 top">
		<div class="container">
			<div class="row">
				<div class="col-sm text-center text-md-left mb-md-0 mb-2 pr-md-4 d-flex topper align-items-center">
					<p class="mb-0 w-100">
						<span class="fa fa-paper-plane"></span>
						<span class="text">informes@aviva.pe</span>
					</p>
				</div>
				<div class="col-sm justify-content-center d-flex mb-md-0 mb-2">
					<div class="social-media">
						<p class="mb-0 d-flex">
							<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
							<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
							<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
							<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-dribbble"><i class="sr-only">Dribbble</i></span></a>
						</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-7 d-flex topper align-items-center text-lg-right justify-content-end">
					<p class="mb-0 register-link"><a href="#" class="btn btn-primary">Reservar una cita</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="py-3">
		<div class="container">
			<div class="row d-flex align-items-start align-items-center px-3 px-md-0">
				<div class="col-md-4 d-flex mb-2 mb-md-0">
					<a class="navbar-brand d-flex align-items-center" href="index.html">
                        <img src="<?php echo base_url('public'); ?>/images/logo.svg" alt="">
                    </a>
				</div>
				<div class="col-md-4 d-flex topper mb-md-0 mb-2 align-items-center">
					<div class="icon d-flex justify-content-center align-items-center">
						<span class="fa fa-map"></span>
					</div>
					<div class="pr-md-4 pl-md-3 pl-3 text">
						<p class="con"><span>Línea gratuita</span> <span>+1 234 456 78910</span></p>
						<p class="con">Atendemos las 24 horas del día</p>
					</div>
				</div>
				<div class="col-md-4 d-flex topper mb-md-0 align-items-center">
					<div class="icon d-flex justify-content-center align-items-center"><span class="fa fa-paper-plane"></span>
					</div>
					<div class="text pl-3 pl-md-3">
						<p class="hr"><span>Ubícanos</span></p>
						<p class="con">Av. Alfredo Mendiola 6301, Los Olivos, Lima.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
		<div class="container d-flex align-items-center">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span> Menu
			</button>
			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav m-auto">
					<li class="nav-item active"><a href="#" class="nav-link">Incio</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Nostros</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Servicios</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Doctores</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Especialidades</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Planes</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Blog</a></li>
					<li class="nav-item"><a href="#" class="nav-link">Contacto</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- END nav -->
	
	<section class="hero-wrap js-fullheight" style="background-image: url('<?php echo base_url('public'); ?>/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row no-gutters slider-text js-fullheight align-items-center justify-content-end" data-scrollax-parent="true">
				<div class="col-lg-6 ftco-animate">
					<div class="mt-5">
						<h1 class="mb-4">ERES MÁS QUE UN PACIENTE, POR ESO, MERECES MÁS QUE UNA CLÍNICA</h1>
						<div class="row">
							<div class="col-md-7 col-lg-10">
								<form action="#" class="appointment-form-intro ftco-animate">
									<div class="d-flex">
										<div class="form-group">
											<div class="form-field">
												<div class="select-wrap">
													<div class="icon"><span class="fa fa-chevron-down"></span></div>
													<select name="" id="" class="form-control">
														<option value="">Especialidades</option>
														<option value="">Neurología</option>
														<option value="">Cardiología</option>
														<option value="">Rayos-X</option>
														<option value="">Dental</option>
														<option value="">Otros servicios</option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<input type="submit" value="Ver especialidad" class="btn-custom form-control py-3 px-4">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section ftco-no-pt ftco-no-pb ftco-services-2 bg-light">
		<div class="container">
			<div class="row d-flex">
				<div class="col-md-7 py-5">
					<div class="py-lg-5">
						<div class="row justify-content-center pb-5">
							<div class="col-md-12 heading-section ftco-animate">
								<h2 class="mb-3">Bienvenido a <span>Aviva</span></h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
								<div class="media block-6 services d-flex">
									<div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-ambulance"></span></div>
									<div class="media-body pl-md-4">
										<h3 class="heading mb-3">Emergencia</h3>
										<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
									</div>
								</div>      
							</div>
							<div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
								<div class="media block-6 services d-flex">
									<div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-ophthalmologist"></span></div>
									<div class="media-body pl-md-4">
										<h3 class="heading mb-3">Doctores calificados</h3>
										<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
									</div>
								</div>      
							</div>
							<div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
								<div class="media block-6 services d-flex">
									<div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-flag"></span></div>
									<div class="media-body pl-md-4">
										<h3 class="heading mb-3">Planes y programas</h3>
										<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
									</div>
								</div>      
							</div>
							<div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
								<div class="media block-6 services d-flex">
									<div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-stethoscope"></span></div>
									<div class="media-body pl-md-4">
										<h3 class="heading mb-3">Tratamiento Médico</h3>
										<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
									</div>
								</div>      
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5 d-flex">
					<div class="appointment-wrap p-4 p-lg-5 d-flex align-items-center">
						<form action="#" class="appointment-form ftco-animate">
							<h3>Formulario de citas</h3>
							<div class="">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nombre">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Apellido">
								</div>
							</div>
							<div class="">
								<div class="form-group">
									<div class="form-field">
										<div class="select-wrap">
											<div class="icon"><span class="fa fa-chevron-down"></span></div>
											<select name="" id="" class="form-control">
                                                <option value="">Especialidades</option>
                                                <option value="">Neurología</option>
                                                <option value="">Cardiología</option>
                                                <option value="">Rayos-X</option>
                                                <option value="">Dental</option>
                                                <option value="">Otros servicios</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Teléfono">
								</div>
							</div>
							<div class="">
								<div class="form-group">
									<div class="input-wrap">
										<div class="icon"><span class="fa fa-calendar"></span></div>
										<input type="text" class="form-control appointment_date" placeholder="Fecha">
									</div>
								</div>
								<div class="form-group">
									<div class="input-wrap">
										<div class="icon"><span class="fa fa-clock-o"></span></div>
										<input type="text" class="form-control appointment_time" placeholder="Hora">
									</div>
								</div>
							</div>
							<div class="">
								<div class="form-group">
									<textarea name="" id="" cols="30" rows="2" class="form-control" placeholder="Mensaje"></textarea>
								</div>
								<div class="form-group">
									<input type="submit" value="Cita" class="btn btn-secondary py-3 px-4">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="ftco-intro img" style="background-image: url(<?php echo base_url('public'); ?>/images/bg_3.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-md-7">
					<h2>Tu salud es nuestra prioridad</h2>
					<p>We can manage your dream building A small river named Duden flows by their place</p>
					<p class="mb-0"><a href="#" class="btn btn-white px-4 py-3">Haga una cita</a></p>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-2">
				<div class="col-md-8 text-center heading-section ftco-animate">
					<h2 class="mb-4">Especialidades en <span>Aviva</span></h2>
				</div>
			</div>
			<div class="row tabulation mt-4 ftco-animate">
				<div class="col-md-3">
					<ul class="nav nav-pills nav-fill d-block w-100">
						<li class="nav-item text-left">
							<a class="nav-link active d-flex align-items-centere py-4" data-toggle="tab" href="#services-1"><span class="flaticon-health flaticon mr-3"></span> <span>Neurología</span></a>
						</li>
						<li class="nav-item text-left">
							<a class="nav-link py-4 d-flex align-items-center" data-toggle="tab" href="#services-2"><span class="flaticon-health flaticon mr-3"></span> <span>Oftalmología</span></a>
						</li>
						<li class="nav-item text-left">
							<a class="nav-link py-4 d-flex align-items-center" data-toggle="tab" href="#services-3"><span class="flaticon-health flaticon mr-3"></span> <span>Magnético nuclear</span></a>
						</li>
						<li class="nav-item text-left">
							<a class="nav-link py-4 d-flex align-items-center" data-toggle="tab" href="#services-4"><span class="flaticon-health flaticon mr-3"></span> <span>Rayos-X</span></a>
						</li>
						<li class="nav-item text-left">
							<a class="nav-link py-4 d-flex align-items-center" data-toggle="tab" href="#services-5"><span class="flaticon-health flaticon mr-3"></span> <span>Quirúrgico</span></a>
						</li>
						<li class="nav-item text-left">
							<a class="nav-link d-flex align-items-centerm py-4" data-toggle="tab" href="#services-6"><span class="flaticon-health flaticon mr-3"></span> <span>Cardiología</span></a>
						</li>
						<li class="nav-item text-left">
							<a class="nav-link d-flex align-items-centerm py-4" data-toggle="tab" href="#services-7"><span class="flaticon-health flaticon mr-3"></span> <span>Clínica Dental</span></a>
						</li>
					</ul>
				</div>

				<div class="col-md-9">
					<div class="tab-content pt-4 pt-md-0 pl-md-3">
						<div class="tab-pane container p-0 active" id="services-1">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-1.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Neurología</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-pane container p-0 fade" id="services-2">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-2.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Oftalmología</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-pane container p-0 fade" id="services-3">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-3.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Magnético nuclear</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-pane container p-0 fade" id="services-4">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-4.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Rayos-X</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-pane container p-0 fade" id="services-5">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-5.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Quirúrgico</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-pane container p-0 fade" id="services-6">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-6.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Cardiología</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-pane container p-0 fade" id="services-7">
							<div class="row">
								<div class="col-md-5 img" style="background-image: url(<?php echo base_url('public'); ?>/images/dept-7.jpg);"></div>
								<div class="col-md-7 text pl-md-4">
									<h3><a href="#">Clínica Dental</a></h3>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
									<ul>
										<li><span class="fa fa-check"></span>The Big Oxmox advised her not to do so</li>
										<li><span class="fa fa-check"></span>Far far away, behind the word mountains</li>
										<li><span class="fa fa-check"></span>Separated they live in Bookmarksgrove</li>
										<li><span class="fa fa-check"></span>She packed her seven versalia</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section bg-light">
		<div class="container-fluid px-5">
			<div class="row justify-content-center mb-5 pb-2">
				<div class="col-md-8 text-center heading-section ftco-animate">
					<h2 class="mb-4">Nuestros doctores calificados</h2>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-6 col-lg-3 ftco-animate">
					<div class="staff">
						<div class="img-wrap d-flex align-items-stretch">
							<div class="img align-self-stretch" style="background-image: url(<?php echo base_url('public'); ?>/images/doc-1.jpg);"></div>
						</div>
						<div class="text text-center">
							<h3 class="mb-2">Dr. Lloyd Wilson</h3>
							<span class="position mb-2">Neurologo</span>
							<div class="faded">
								<p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
								<ul class="ftco-social text-center">
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-google"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3 ftco-animate">
					<div class="staff">
						<div class="img-wrap d-flex align-items-stretch">
							<div class="img align-self-stretch" style="background-image: url(<?php echo base_url('public'); ?>/images/doc-2.jpg);"></div>
						</div>
						<div class="text text-center">
							<h3 class="mb-2">Dr. Rachel Parker</h3>
							<span class="position mb-2">Oftalmologo</span>
							<div class="faded">
								<p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
								<ul class="ftco-social text-center">
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-google"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3 ftco-animate">
					<div class="staff">
						<div class="img-wrap d-flex align-items-stretch">
							<div class="img align-self-stretch" style="background-image: url(<?php echo base_url('public'); ?>/images/doc-3.jpg);"></div>
						</div>
						<div class="text text-center">
							<h3 class="mb-2">Dr. Ian Smith</h3>
							<span class="position mb-2">Dentista</span>
							<div class="faded">
								<p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
								<ul class="ftco-social text-center">
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-google"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3 ftco-animate">
					<div class="staff">
						<div class="img-wrap d-flex align-items-stretch">
							<div class="img align-self-stretch" style="background-image: url(<?php echo base_url('public'); ?>/images/doc-4.jpg);"></div>
						</div>
						<div class="text text-center">
							<h3 class="mb-2">Dr. Alicia Henderson</h3>
							<span class="position mb-2">Pediatra</span>
							<div class="faded">
								<p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
								<ul class="ftco-social text-center">
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-google"></span></a></li>
									<li class="ftco-animate"><a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section testimony-section img" style="background-image: url(<?php echo base_url('public'); ?>/images/bg_4.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row justify-content-center pb-3">
				<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
					<span class="subheading">Leer testimonios</span>
					<h2 class="mb-4">Nuestros pacientes dicen</h2>
				</div>
			</div>
			<div class="row ftco-animate justify-content-center">
				<div class="col-md-12">
					<div class="carousel-testimony owl-carousel ftco-owl">
						<div class="item">
							<div class="testimony-wrap py-4 pb-5 d-flex justify-content-between">
								<div class="user-img" style="background-image: url(<?php echo base_url('public'); ?>/images/person_1.jpg)">
									<span class="quote d-flex align-items-center justify-content-center">
										<i class="fa fa-quote-left"></i>
									</span>
								</div>
								<div class="text">
									<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia</p>
									<p class="name">Jeff Freshman</p>
									<span class="position">Paciente</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4 pb-5 d-flex justify-content-between">
								<div class="user-img" style="background-image: url(<?php echo base_url('public'); ?>/images/person_2.jpg)">
									<span class="quote d-flex align-items-center justify-content-center">
										<i class="fa fa-quote-left"></i>
									</span>
								</div>
								<div class="text">
									<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia</p>
									<p class="name">Jeff Freshman</p>
									<span class="position">Paciente</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4 pb-5 d-flex justify-content-between">
								<div class="user-img" style="background-image: url(<?php echo base_url('public'); ?>/images/person_3.jpg)">
									<span class="quote d-flex align-items-center justify-content-center">
										<i class="fa fa-quote-left"></i>
									</span>
								</div>
								<div class="text">
									<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia</p>
									<p class="name">Jeff Freshman</p>
									<span class="position">Paciente</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4 pb-5 d-flex justify-content-between">
								<div class="user-img" style="background-image: url(<?php echo base_url('public'); ?>/images/person_1.jpg)">
									<span class="quote d-flex align-items-center justify-content-center">
										<i class="fa fa-quote-left"></i>
									</span>
								</div>
								<div class="text">
									<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia</p>
									<p class="name">Jeff Freshman</p>
									<span class="position">Paciente</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-wrap py-4 pb-5 d-flex justify-content-between">
								<div class="user-img" style="background-image: url(<?php echo base_url('public'); ?>/images/person_3.jpg)">
									<span class="quote d-flex align-items-center justify-content-center">
										<i class="fa fa-quote-left"></i>
									</span>
								</div>
								<div class="text">
									<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia</p>
									<p class="name">Jeff Freshman</p>
									<span class="position">Paciente</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="ftco-footer">
		<div class="container mb-5 pb-4">
			<div class="row">
				<div class="col-lg col-md-6">
					<div class="ftco-footer-widget">
						<h2 class="ftco-heading-2 logo d-flex align-items-center">
                            <a href="#">
                                <img src="<?php echo base_url('public'); ?>/images/logo.svg" alt="">
                            </a>
                        </h2>
						<ul class="ftco-footer-social list-unstyled mt-4">
							<li><a href="#"><span class="fa fa-twitter"></span></a></li>
							<li><a href="#"><span class="fa fa-facebook"></span></a></li>
							<li><a href="#"><span class="fa fa-instagram"></span></a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg col-md-6">
					<div class="ftco-footer-widget">
						<h2 class="ftco-heading-2">Especialidades</h2>
						<ul class="list-unstyled">
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Neurología</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Oftalmología</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Magnético Nuclear</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Rayos-X</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Quirurgico</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Cardiología</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Dental</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg col-md-6">
					<div class="ftco-footer-widget">
						<h2 class="ftco-heading-2">Enlaces</h2>
						<ul class="list-unstyled">
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Home</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Nosotros</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Especialidades</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Doctores</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Blog</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Planes</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Contacto</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg col-md-6">
					<div class="ftco-footer-widget">
						<h2 class="ftco-heading-2">Servicios</h2>
						<ul class="list-unstyled">
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Emergencia</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Doctores Calificados</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Planes y programas</a></li>
							<li><a href="#"><span class="fa fa-chevron-right mr-2"></span>Tratamientos Médicos</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg col-md-6">
					<div class="ftco-footer-widget">
						<h2 class="ftco-heading-2">Tienes una pregunta?</h2>
						<div class="block-23 mb-3">
							<ul>
								<li><span class="fa fa-map-marker mr-3"></span><span class="text">Av. Alfredo Mendiola 6301, Los Olivos, Lima.</span></li>
								<li><a href="#"><span class="fa fa-phone mr-3"></span><span class="text">+2 392 3929 210</span></a></li>
								<li><a href="#"><span class="fa fa-paper-plane mr-3"></span><span class="text">informes@aviva.pe</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid bg-primary py-5" style="display:none;">
			<div class="row">
				<div class="col-md-12 text-center">
					
					<p class="mb-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
					</div>
				</div>
			</div>
	</footer>
		
		

		<!-- loader -->
		<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


    <!-- chatbot -->
    
    <div class="wrapper box_chat">
        <div class="title">Avivabot</div>
        <div class="form">
            <div class="bot-inbox inbox">
                <div class="icon">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </div>
                <div class="msg-header">
                    <p>Bienvenido a la clínica Aviva</p>
                </div>
            </div>
        </div>
        <div class="typing-field">
            <div class="input-data">
                <input id="data" type="text" placeholder="Escribe tu consulta aquí.." required="required">
				<input type="hidden" id="data_idr" value="">
				<input type="hidden" id="data_respuesta" value="">
				<input type="hidden" id="data_orden" value="">
				<input type="hidden" id="data_asunto" value="">
                <button id="send-btn">Enviar</button>
            </div>
        </div>
    </div>
    <!-- chatbot -->

		<script src="<?php echo base_url('public'); ?>/js/jquery.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery-migrate-3.0.1.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/popper.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery.easing.1.3.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery.waypoints.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery.stellar.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/owl.carousel.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery.animateNumber.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/jquery.timepicker.min.js"></script>
		<script src="<?php echo base_url('public'); ?>/js/scrollax.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
		<script src="<?php echo base_url('public'); ?>/js/google-map.js"></script>
		
		<script src="<?php echo base_url('public'); ?>/js/main.js"></script>
		
    

        <script>
        $(document).ready(function(){
			var confirmacion = '';
            $("#send-btn").on("click", function(){

                var consulta = $("#data").val();
				var data_idr = $("#data_idr").val();
				var data_respuesta = $("#data_respuesta").val();
				var data_orden = $("#data_orden").val();
				var data_asunto = $("#data_asunto").val();
				var parametros = {"value":consulta, "idr":data_idr, "respuesta":data_respuesta, "orden":data_orden, "asunto":data_asunto};
				console.log(parametros);
				
				if(data_respuesta == 'NO')
					window.location.reload();

                $msg = '<div class="user-inbox inbox"><div class="msg-header"><p>'+ consulta +'</p></div></div>';
                $(".form").append($msg);
				$("#data").val('');
                
                // start ajax code
                $.ajax({
                    url: '<?php echo base_url(); ?>messagebot',
                    type: 'POST',
                    data: parametros,
                    success: function(result){
						let resultjson = JSON.parse(result);
						$opciones = '';
						if(resultjson.id != 0)
						{
                			$("#data_idr").val(resultjson.id);
						}
						if(resultjson.orden != '')
						{
                			$("#data_orden").val(resultjson.orden);
						}
						if(resultjson.asunto != '')
						{
							$("#data_asunto").val(resultjson.asunto);
							if(resultjson.asunto == 'comprobante')
							{								
								$opciones += '<div>';
								$opciones += '<a class="enld" href="<?php echo base_url(); ?>downloadpdf/'+data_idr+'">Descargar comprobante</a>';
								$opciones += '</div>';
							}
						}
						if(resultjson.opciones != '')
						{
							$opciones += '<div>';
							$.each(resultjson.opciones, function (indexInArray, valueOfElement) { 
								$opciones += '<button class="sel_opcion" value="'+valueOfElement+'">'+valueOfElement+'</button>';
							});
							$opciones += '</div>';
						}
						
						console.log(resultjson);
                        $replay = '<div class="bot-inbox inbox"><div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div><div class="msg-header"><p>'+ resultjson.respuesta +'</p>'+$opciones+'</div></div>';
                        $(".form").append($replay);
                        // when chat goes down the scroll bar automatically comes to the bottom
                        $(".form").scrollTop($(".form")[0].scrollHeight);
						$('.sel_opcion').on('click', function(){
							let valor = $(this).val();
							$("#data").val(valor);
							$("#data_respuesta").val(valor);
							if(valor == 'SI' || valor == 'NO')
								confirmacion = valor;
						});
						if(confirmacion != '')
						{
							$("#data_idr").val('');
							$("#data_respuesta").val('');
							$("#data_orden").val('');
							$("#data_asunto").val('');
						}
                    }
                });
            });
            $("#data").on('keypress', function(event){
                if (event.which == 13 || event.keyCode == 13) {
                    $("#send-btn").click();
                    return false;
                }
			});
        });
    </script>
    
    
    </body>
	</html>