<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="utf-8">
	<title>PDF Created</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}
h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 20px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}
.box {
    border: 2px solid #6f6f6f;
    max-width: 500px;
    padding: 30px;
}
p {
    margin-top: 3px;
}
h4 {
    margin-bottom: 0;
}
</style>
</head><body>
<div class="box">
<h1>Clínica Aviva</h1>

<p>Estimado(a) <b><?php echo strtoupper($result->paciente); ?></b>, le recordamos que usted tiene reservado una cita médica en nuestra clínica. A continuación especificamos los detalles de la reserva.</p>

<h3>Detalles del paciente</h3>
<h4>Paciente</h4>
<p><?php echo strtoupper($result->paciente); ?></p>
<br>
<h4>N° documento</h4>
<p><?php echo $result->dni; ?></p>
<br>
<h3>Detalles de la reserva</h3>
<h4>Especialidad</h4>
<p><?php echo $result->especialidad; ?></p>
<br>
<h4>Fecha</h4>
<p><?php echo date('d-m-Y',strtotime($result->fecha)); ?></p>
<br>
<h4>Horario</h4>
<p><?php echo $result->horario; ?></p>
<br>
<br>
<p>Recuerda que debes realizar el pago correspondiente por nuestros canales de atención previamente a la fecha indicada en tu reservación para que puedas ser atendido en nuestra clínica.</p>
</div>
</body></html>