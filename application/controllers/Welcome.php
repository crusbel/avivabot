<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('inicio');
	}

	public function message()
	{
		$getMesg = $this->input->post('value');
		$getIdr = $this->input->post('idr');
		$getRespuesta = $this->input->post('respuesta');
		$getOrden = $this->input->post('orden');
		$getAsunto = $this->input->post('asunto');

		if(!empty($getOrden))
		{
			$query = $this->db->select('replies, orden, opciones, asunto')->from('chatbot')->where('id',$getOrden)->limit(1)->get();
		}
		else
		{
			$query = $this->db->select('replies, orden, opciones, asunto')->from('chatbot')->like('queries',$getMesg)->limit(1)->get();
		}
		$query = $query->result();

		$id = (!empty($getIdr)) ? $getIdr : 0;
		$opciones = '';
		$orden = '';
		$asunto = '';

		if(count($query) > 0)
		{
			if(empty($getIdr) && !is_null($query[0]->orden))
			{
				$reserva = $this->db->insert('reservas',array('estado'=>1));
				$getid = $this->db->insert_id();
				$id = $getid;
			}
			if(!is_null($query[0]->opciones))
			{
				$opciones = json_decode($query[0]->opciones);
			}
			if(!empty($query[0]->orden))
			{
				$orden = $query[0]->orden;
			}
			if(!empty($query[0]->asunto))
			{
				$asunto = $query[0]->asunto;
			}
			if(!empty($getIdr))
			{
				if(!empty($getAsunto))
				{
					switch ($getAsunto) {
						case 'especialidad':
							if(!empty($getRespuesta))
								$this->db->where('id',$getIdr)->update('reservas',array('especialidad'=>$getRespuesta));
							break;
						case 'fecha':
							if(!empty($getRespuesta))
								$this->db->where('id',$getIdr)->update('reservas',array('fecha'=>date('Y-m-d',strtotime($getRespuesta))));
							break;
						case 'horario':
							if(!empty($getRespuesta))
								$this->db->where('id',$getIdr)->update('reservas',array('horario'=>date('H:i:s',strtotime($getRespuesta))));
							break;
						case 'dni':
							if(!empty($getRespuesta))
								$this->db->where('id',$getIdr)->update('reservas',array('dni'=>$getMesg));
							break;
						case 'paciente':
							if(!empty($getRespuesta))
								$this->db->where('id',$getIdr)->update('reservas',array('paciente'=>$getMesg));
							break;
						case 'confirmacion':
							if(!empty($getRespuesta))
								$this->db->where('id',$getIdr)->update('reservas',array('estado'=>($getRespuesta == 'SI') ? 2 : 0));
							break;
						
						default:
							break;
					}
				}

			}
			$replay = $query[0]->replies;
			echo json_encode(array( 'id'=> $id, 'respuesta' => $replay, 'opciones' => $opciones, 'orden' => $orden, 'asunto' => $asunto));
		}
		else
		{
			echo json_encode(array( 'id'=> $id, 'respuesta' => "¡Lo siento, no puedo entenderte!", 'opciones' => $opciones, 'orden' => $orden, 'asunto' => $asunto));
		}

	}

	public function downloadpdf($id = 0)
	{
	    $this->load->library('html2pdf');
	    $this->html2pdf->folder('./assets/pdfs/');
	    $this->html2pdf->filename('Reservación de cita - Aviva.pdf');
	    $this->html2pdf->paper('a4', 'portrait');
	    
		$query = $this->db->select('*')->from('reservas')->where('id',$id)->where('estado',2)->limit(1)->get();
		$result = $query->row();
	    $data = array(
	    	'result' => $result
	    );
	    
	    $this->html2pdf->html($this->load->view('pdf', $data, true));
	    
	    if($this->html2pdf->create('download')) {
	    	echo 'PDF saved';
	    }
	}
	public function loadpdf($id = 0)
	{
		$query = $this->db->select('*')->from('reservas')->where('id',$id)->where('estado',2)->limit(1)->get();
		$result = $query->row();
	    $data = array(
	    	'result' => $result
	    );
		$this->load->view('pdf', $data);
	}
}
